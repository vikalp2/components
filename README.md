# Components

Shared components that we would be using across projects, and hosted on [bit.cloud](https://bit.cloud/).

## Getting Started

To get started straight away run

```shell
$ bit install
$ bit start
```

and open [localhost:3000](http://localhost:3000).

It may take a while to build the first time you run this command as it is building the whole User Interface for your development environment.

## Bit.dev Commands

- Create a new react component

  ```shell
  $ bit create react <component-name> # eg, bit create react ui/button
  ```

- Create a new react-native component

  ```shell
  $ bit create react-native <component-name>  # eg, bit create react-native ui/button
  ```

- Start Bit Gallery on localhost

  ```shell
  $ bit start
  ```

- Check out any changes in components (new or modified)

  ```shell
  $ bit status
  ```

- Create a branch to purpose changes to Component

  ```shell
  $ bit lane create bit <component-name>  # eg, bit lane create feat/new-button
  ```

- Commit with message

  ```shell
  $ bit snap --all --message "feat: new ui/button component"
  ```

- Prepare for releasing Component changes, by updating version & tagging

  ```shell
  $ bit tag --all --message "new initial setup"
  ```

- Push your changes online

  ```shell
  $ bit export
  ```

### Remove component

This removes the components from the bitmap as well as removes the files.

```shell
$ bit remove "ui/*" --delete files
```

## Project Structure

- **workspace.jsonc**

This is the main configuration file of your bit workspace. Here you can modify the workspace name and icon as well as default directory and scope. It is where dependencies are found when you install anything. It is also where you register aspects, bit extensions as well as apply the environments for your components. This workspace has been setup so that all components use the React env. However you can create other components and apply other envs to them such as node, html, angular and aspect envs.

- **.bitmap**

This is an auto-generated file and includes the mapping of your components. There is one component included here. In order to remove this component you can run the following command.

- **Demo Components**

A folder (unless the --empty flag was used) containing demo components are included in this workspace. These components are used to demonstrate the different features of Bit. If you would like to remove these components you can run the following command.
